const StringUtils = {
	split(str , separator){
		if(!str || str.length == 0){
			return []
		}
		return str.split(separator)
	},

	join(array , separator){
		if(!array || array.length == 0){
			return ''
		}
		return array.join(separator)
	}
}

export default StringUtils