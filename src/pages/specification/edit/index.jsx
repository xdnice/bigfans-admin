import React from 'react';
import {Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete , Switch} from 'antd';
import {Breadcrumb} from 'antd';
import {Card} from 'antd';
import SpecEditFrom from './form'
import HttpUtils from 'utils/HttpUtils'

class SpecEditPage extends React.Component {

    render() {
        return (
            <div>
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>规格管理</Breadcrumb.Item>
                    <Breadcrumb.Item>创建规格</Breadcrumb.Item>
                </Breadcrumb>
                <Card bordered={false}>
                    <SpecEditFrom history={this.props.history} specId={this.props.match.params.id} />
                </Card>
            </div>
        );
    }
}

export default SpecEditPage;