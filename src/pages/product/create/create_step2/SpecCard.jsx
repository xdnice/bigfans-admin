import React from 'react';
import {Card} from 'antd';
import SpecEditableTable from './SpecEditableTable'
import ProductPicturesWall from './ProductPicturesWall'

class SpecCard extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Card title="红色 4G" style={{margin: "0px 0px 40px 0px"}} extra={<a href="#">收起</a>}>
                <ProductPicturesWall form={this.props.form} prodIndex={this.props.prodIndex}/>
                <SpecEditableTable
                               pagination={false}
                               form={this.props.form}
                               specs={this.props.specs}
                               prodIndex={this.props.prodIndex}/>
            </Card>
        )
    }
}

export default SpecCard;