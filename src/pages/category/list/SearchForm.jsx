import React from 'react';
import {Button , Form , Row, Col , Input,Cascader , Select} from 'antd'

class SearchForm extends React.Component {

	state = {
		parentCats : []
	}

	handleSearch = () => {

	}

	handleReset = () => {
		this.props.form.resetFields();
	}

	render () {
		const {getFieldDecorator} = this.props.form;
		const formItemLayout = {
	      labelCol: { span: 5 },
	      wrapperCol: { span: 19 },
	    };
		return (
			<Form className="ant-advanced-search-form" onSubmit={e => {this.handleSubmit(e)}}>
				<Row gutter={40}>
					<Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="分类" >
                            {getFieldDecorator('parentId')(
                                <Cascader placeholder='按分类查找' options={this.state.parentCats} changeOnSelect/>
                            )}
                        </Form.Item>
	                </Col>
	                <Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="级别" >
                            {getFieldDecorator('level')(
                                <Select>
                                    <Select.Option value="1">一级分类</Select.Option>
                                    <Select.Option value="2">二级分类</Select.Option>
                                    <Select.Option value="3">三级分类</Select.Option>
                                </Select>
                            )}
                        </Form.Item>
	                </Col>
	                <Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="分类名称" >
		                    {getFieldDecorator('name4')(
		                        <Input/>
		                    )}
		                </Form.Item>
	                </Col>
                </Row>
                <Row>
                	<Col span={24} style={{ textAlign: 'right' }}>
			            <Button type="primary" htmlType="submit">Search</Button>
			            <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
			              Clear
			            </Button>
			        </Col>
                </Row>
			</Form>

			)
	}
}

const CategorySearchForm = Form.create()(SearchForm);
export default CategorySearchForm;
